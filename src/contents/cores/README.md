# Core Sources

`mednafen_gba_libretro.so` - [Beetle GBA](https://github.com/libretro/beetle-gba-libretro)

`2048_libretro.so` - [Libretro 2048](https://github.com/libretro/libretro-2048)

`snes9x_libretro.so` - [snes9x](https://github.com/snes9xgit/snes9x)

`quicknes_libretro.so` - [QuickNES](https://github.com/kode54/QuickNES)